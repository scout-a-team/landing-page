[![Netlify Status](https://api.netlify.com/api/v1/badges/95ff5e52-ed63-4b09-8ab0-e5df31067ea2/deploy-status)](https://app.netlify.com/sites/scout-a-team/deploys)
# Scout a Team
This is the landing page for Scout a Team. It is a static page that makes API calls to the backend service in order to provide maximum speed and ease of use.

The theme is a modified version of [Hugo Lime by UICardio](https://github.com/uicardiodev/hugo-lime).
