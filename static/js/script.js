$(document).ready(function(){
  // Activating tooltip
  $('[data-toggle="tooltip"]').tooltip();

  // Smooth scroll
  $('a').click(function(){
    let hash = $(this).attr('href').split('#')[1];
    $('html, body').animate({ scrollTop: $('#' + hash).offset().top }, 500);
    return false;
  });

});